/*******************************/
/* Author: Philipp Borkowitsch */
/* Date: 14.02.19  *************/
/* FileName: DOUBLELists.c *****/
/*******************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

// Struct: definiert Double-Linked-List
struct student {
    char name[40+1];
    int id;
    char course [15+1];
    struct student* next;
    struct student* prev;
};

void noEntry(){
    printf("Keine Eintr\x84ge vorhanden\nEnter um fortzufahren");
    fflush(stdin);
    while( getchar() != '\n' );
    printf("\n\n\n");
    return;
}

// Returnt den Anfang der Liste aus
struct student* goBack(struct student* ptr){
    while (ptr -> prev){
        ptr = (ptr -> prev);
    }
    return (ptr);
}

void getRidOfBackslashN(char string[]){
    int i = 0;
    while (string[i] != '\n'){i++;}
    string[i] = '\0';
    return;
}

// Funktion setzt einen neuen Eintrag ans Ende der Liste
struct student* addStudent(struct student* ptr){

    char input[42];

    // Beansprucht Speicherplatz für neuen Studenten
    struct student* newMeme = (struct student*)malloc(sizeof(struct student));

    // Belegt neuen Speicherplatz mit den eingegebenen Werten
    printf("****NEUER STUDENT****\nNamen eingeben: ");
    fgets((newMeme -> name),sizeof(newMeme -> name),stdin);
    getRidOfBackslashN(newMeme->name);

    fflush(stdin);

    printf("ID eingeben: ");
    fgets(input, sizeof(input), stdin);
    newMeme -> id = atoi(input);

    fflush(stdin);

    // Wenn etwas anderes als Ziffern eingegeben wurde, wird eine neue Eingabe verlangt
    while (newMeme -> id == 0){
        printf("FEHLER: ID ung\x81ltig!\nNeue ID eingeben: ");
        fgets(input, sizeof(input), stdin);
        newMeme -> id = atoi(input);
        fflush(stdin);
    }

    printf("Kurs eingeben: ");
    fgets((newMeme -> course),sizeof(newMeme -> course),stdin);
    getRidOfBackslashN(newMeme->course);
    fflush(stdin);

    L: 
    // falls Liste schon existiert
    if (ptr){
        // Zum Anfang der Liste springen
        ptr = goBack(ptr);
        // Gleichzeitiges überprüfen ob die ID schon vergeben ist und "scrollen" zum letzten Eintrag.
        while(ptr -> next){
            if ((ptr -> id) == (newMeme -> id)){
                printf("FEHLER: ID schon vergeben!\nNeue ID eingeben:");
                    fgets(input, sizeof(input), stdin);
                    newMeme -> id = atoi(input);
                    fflush(stdin);
                // Falls Id schon existiert muss nochmal von vorne gecheckt werden, daher mit Label L gelöst
                goto L;
            }
            ptr = ptr -> next;
        }
        // Anbindung an den Rest der Liste
        newMeme -> next = NULL;
        newMeme -> prev = ptr;
        ptr -> next = newMeme;
    // falls Liste neu kreiert wird
    } else {
        newMeme -> next = NULL;
        newMeme -> prev = NULL;
    }
    printf("\n\n****STUDENT HINZUGEF\x9AGT****\nEnter, um fortzufahren");
    while( getchar() != '\n' );
    printf("\n\n\n");
    // Ausgeben der Adresse des neuen Eintrags
    return (newMeme);
}

// Ausgeben aller Daten.
void printData(struct student* ptr, int mode){
    // Zunächst den Anfang der Liste finden
    ptr = goBack(ptr);
    // Dann alle Einträge ausgeben
    while (ptr){
        printf("------\nName: %s\nID: %d\nKurs: %s\n", (ptr -> name), (ptr -> id), (ptr -> course));
        if (mode == 1){
            // Debug mode gibt zusätzliche Infos zu den Pointern aus
            printf("Pointer: %p\nPrev: %p\nNext: %p\n", ptr, (ptr -> prev), (ptr -> next));
        }
        ptr = (ptr -> next);
    }
    printf("-----\n");
    fflush(stdin);
    printf("\n****LISTENENDE****\nEnter, um fortzufahren");
    while( getchar() != '\n' );
    printf("\n\n\n");
}

// Löschen eines Eintrags
struct student* deleteStudent(struct student* ptr){

    int id;
    // Hilfs-Adresse zum Löschen des Eintrags
    struct student* dump;

    printf("****L\x99SCHEN****\nGeben Sie die ID des zu l\x94schenden Eintrags ein: ");
    scanf("%d", &id);

    //Scrollen zum ersten Eintrag
    ptr = goBack(ptr);
    // Durchsuchen der Liste nach dem Eintrag mit der ID
    while ((ptr -> id) != id){
    //Fehler falls Eintrag nicht existiert
        if ((ptr -> next) == NULL){
            fflush(stdin);
            printf("Eintrag existiert nicht!\nEnter, um fortzufahren");
            while( getchar() != '\n' );
            printf("\n\n\n");
            return (ptr);
        }
        ptr = (ptr -> next);
    }
    //Falls der erste Eintrag gelöscht werden sollte
    if (ptr -> prev == NULL && ptr -> next){
            ((ptr -> next) -> prev) = NULL;
    //Wenn ich das einzige Element der Liste lösche
        } else if (ptr-> next == NULL){
            free(ptr);
            ptr = NULL;
    //Anderfalls Verlinkung des Vorg�ngers mit dem Nachfolger
        } else {
            ((ptr -> prev) -> next) = (ptr -> next);
            ((ptr -> next) -> prev) = (ptr -> prev);
            dump = ptr;
            ptr = (ptr -> next);
            //Erst hier löschen des Eintrags
            free(dump);
    } 
    printf("\n\n****EINTRAG GEL\x99SCHT****\nEnter, um fortzufahren");
    while( getchar() != '\n' );
    printf("\n\n\n");
    return(ptr);
}

// Tauscht den ptr-Eintrag mit dem ptr->next-Eintrag
int swapStudents(struct student* ptr){

    // Hilfspointer
    struct student* prev = ptr -> prev;
    struct student* next = ptr -> next;
    struct student* nextx2 = ptr -> next -> next;

    // Tauschen beginnt hier, insgesamt werden bis zu vier Einträge verändert.
    ptr -> prev = ptr -> next;
    ptr -> next = nextx2;

    // Nur wenn nicht der erste Eintrag getauscht wird
    if (prev){
    prev -> next = next;
    }

    // Nur wenn die Liste nach dem nächsten Eintrag noch nicht zu Ende ist
    if (nextx2){
    nextx2 -> prev = ptr;
    }

    next -> next = ptr;
    next -> prev = prev;

    return (1);
}

// Vergleicht die Buchstaben an i-ter Stelle des Namen-Strings
int stringVergleich(struct student* ptr, int i){
    // Zuerst der Fall für gleiche Groß/Kleinschreibung und gleiche Buchstaben
    if ((isupper(ptr -> name[i]) != islower(ptr -> next -> name[i])) && ptr -> name[i] == ptr -> next -> name[i]){
        return (0);
    // hier falls ptr -> name klein geschrieben ist und next -> name nicht
    } else if (islower(ptr -> name[i]) && isupper(ptr -> next -> name[i]) && ((ptr -> name[i]) == (ptr -> next -> name[i] + 32))){
        return (0);
    // vice versa
    } else if (isupper(ptr -> name[i]) && islower(ptr -> next -> name[i]) && ((ptr -> name[i] + 32) == (ptr -> next -> name[i]))){
        return (0);
    // hier Vergleich ob ptr -> name weiter hinten im Alphabet steht aber kleingeschrieben ist
    } else if (islower(ptr -> name[i]) && isupper(ptr -> next -> name[i]) && ((ptr -> name[i]) > (ptr -> next -> name[i] + 32))){
        return (1);
    // vice versa
    } else if (isupper(ptr -> name[i]) && islower(ptr -> next -> name[i]) && ((ptr -> name[i] + 32) > (ptr -> next -> name[i]))){
        return (1);
    // gleiche Groß/Kleinschreibung
    } else if ((isupper(ptr -> name[i]) != islower(ptr -> next -> name[i])) && ptr -> name[i] > ptr -> next -> name[i]){
        return (1);
    // falls richtige Anordnung schon vorhanden
    } else {
        return (2);
    }
    // ANMERKUNG: Man hätte auch drei große, mit || verlinkte Fälle schreiben können
    // Ich finde diese Lösung aber übersichtlicher
    // Bitte keine Punkte abziehen wegen zu viel Code :)
}

void sortStudents(struct student* ptr){
    // i zählt die Anzahl der Vertauschungen.
    int i = 1;
    // j ist für die Indexposition beim Namensvergleich
    int j = 0;
    // input wird im Menü verwendet
    int input;

    P: 
    printf("****SORTIEREN****\nSoll nach Namen oder ID sortiert werden?\n(1)-Name\n(2)-ID\n");
    scanf("%d", &input);
    fflush(stdin);

    switch (input){
        // Sortieren nach Namen
        case 1: while (i != 0){
                i = 0;
                // An den Anfang der Liste springen 
                ptr = goBack(ptr);
                // BubbleSort
                while (ptr -> next){
                    j = 0;
                    // Falls der erste Buchstabe gleich ist wird der nächste betrachtet
                    while (stringVergleich(ptr, j) == 0){
                        j++;
                    }
                    if (stringVergleich(ptr, j) == 1){
                        // falls i nach einem durchgang 0 sein sollte, also nichts getauscht wurde, ist die Sortierung zu Ende
                        i = i + swapStudents(ptr);
                    } else {
                    ptr = ptr -> next;
                    }
                }
            } 
            break;
        // Sortieren nach ID
        case 2: while (i != 0){
                i = 0; 
                ptr = goBack(ptr);
                // BubbleSort
                while (ptr -> next){
                    if ((ptr -> id) > (ptr -> next -> id)){
                        i = i + swapStudents(ptr);
                    } else {
                    ptr = ptr -> next;
                    }
                }
            }
            break;
        default: printf("Ung\x81ltige Eingabe...\n");
                fflush(stdin);
                goto P;

    }
    printf("\n\n****LISTE SORTIERT****\nEnter, um fortzufahren");
    while( getchar() != '\n' );
    printf("\n\n\n");
    return;
}

// Speichert alle Einträge in students.txt
void saveData(struct student* ptr){
    
    FILE* curfile; 
    char filename[42];

    printf("****SPEICHERN****\nDateiname eingeben (Endung: .txt, Leerzeichen vermeiden): ");
    scanf("%s", filename);
    fflush(stdin);
      
    // Öffnet Datei oder erstellt sie
    curfile = fopen(filename, "w"); 
    // falls Datei nicht schreibbar
    if (curfile == NULL) { 
        printf("\nError: Datei nicht schreibbar\n"); 
    } else {
        // Zunächst an den Listenanfang scrollen
        ptr = goBack(ptr);
        // Hier werden die einzelnen Einträge in die Datei gelesen
        while (ptr){
            fprintf(curfile, "%s\n%d\n%s\n", ptr -> name, ptr -> id, ptr -> course);
            ptr = ptr -> next;
        }
        // Datei wird geschlossen
        fclose(curfile);
        printf("\n\n****LISTE GESPEICHERT****\n");
    }
    printf("Enter, um fortzufahren");
    while( getchar() != '\n' );
    printf("\n\n\n");
    return;
}

// Importiert die Studenten aus einer Datei
struct student* loadFile(struct student* ptr){
    
    char fileName[42];
    printf("****LADEN****\nBitte Dateinamen eingeben (Endung: .txt): ");
    scanf("%s", fileName);
    fflush(stdin);

    // Öffnet die Datei im read-Modus
    FILE* curfile = fopen(fileName, "r");
    char input[42];
    struct student* ptr2 = NULL;

    // Error falls Datei nicht gelesen werden kann
    if (curfile == NULL) { 
        printf("\nError: Datei nicht lesbar\n");
    } else {
        // Löschen der vorhandenen Einträge, falls Einträge vorhanden
        if (ptr){
            while(ptr -> next){
                ptr = ptr -> next;
            }
            while(ptr-> prev){
                ptr = ptr -> prev;
                free(ptr -> next);
            }
            free(ptr);
        }
        // Erzeugen von neuen Einträgen
        ptr = (struct student*)malloc(sizeof(struct student));

        // Erster Name wird außerhalb der Schleife gelesen, damit die Schleife abgebrochen werden kann
        // fall das EoF bereits erreicht wurde. Verhindert einen Nonsense-Eintrag
        fgets(ptr -> name, (sizeof(ptr->name)), curfile);
        getRidOfBackslashN(ptr->name);

        while (feof(curfile) == 0){

            fgets(input, sizeof(input), curfile);
            ptr -> id = atoi(input);    
            fgets(ptr -> course, (sizeof(ptr->course)), curfile);
            getRidOfBackslashN(ptr->course);  

            // Verknüpfen zum Rest der Liste
            ptr -> next = NULL;
            ptr -> prev = ptr2;                    
            if (ptr2){
                ptr2 -> next = ptr;
            }
            ptr2 = ptr;
            
            // bei der letzten Iteration wird hier ein überflüssiger Struct generiert
            ptr = (struct student*)malloc(sizeof(struct student));

            fgets(ptr -> name, (sizeof(ptr->name)), curfile);
            getRidOfBackslashN(ptr->name);
        }
        // Überflüssiger Eintrag wird gefreed
        free(ptr);
        printf("\n\n****LISTE GELADEN****\n");
    }
    // Schließen der Datei
    fclose(curfile);

    printf("Enter, um fortzufahren");
    while( getchar() != '\n' );
    printf("\n\n\n");

    // Ausgabe des letzten gültigen Eintrags
    return ptr2;
}


/////////////////////////////////////////////
//      ___  ___       ___   _   __   _    //
//     /   |/   |     /   | | | |  \ | |   //
//    / /|   /| |    / /| | | | |   \| |   //
//   / / |__/ | |   / / | | | | | |\   |   //
//  / /       | |  / /  | | | | | | \  |   //
// /_/        |_| /_/   |_| |_| |_|  \_|   //
//                                         //
/////////////////////////////////////////////
void main(int argc, char** argv){

    struct student* ptr;

    int input;
    int i;
    int mode;


    //Bei Start auf NULL setzen
    ptr = NULL;

    if (argc > 1){
        for (i = 1; i <= argc; i++){
            printf("%s", argv[i]);
            if (strcmp(argv[i-1], "-debug\0")==0){
                // Debug mode
                printf("\nDEBUG MODE ENTERED\n");
                mode = 1;
            }
        }
    }
    printf("******************************************\nWillkommen zum Datenbankprojekt der DHBW!\n******************************************\n");

    // INTERFACE
    while (0==0){
        printf("****HAUPTMEN\x9A****\nW\x84hlen Sie aus folgenden Funktionen:\n(1)-Einen Studenten hinzuf\x81gen\n(2)-Alle Studenten anzeigen\n(3)-Einen Studenten l\x94schen\n(4)-Datensatz speichern\n(5)-Datensatz laden\n(6)-Die Liste sortieren\n(7)-Das Programm beenden\n");
        scanf("%d", &input);
        fflush(stdin);
            switch(input) {
                        // Neuen Eintrag hinzufügen
                case 1: ptr = addStudent(ptr);
                        break;

                        // Datensatz ausgeben falls Liste vorhanden
                case 2: if (ptr == NULL) {
                            noEntry();
                        } else {
                        printData(ptr, mode);
                        } break;

                        // Checken ob ich überhaupt eine Liste habe
                case 3: if (ptr == NULL) {
                            noEntry();
                        } else {
                        // Falls ja kann gelöscht werden
                           ptr = deleteStudent(ptr);
                        } break;

                        // Save-Funktion wird nur dann ausgeführt, wenn bereits Daten bestehen
                case 4: if (ptr == NULL) {
                            noEntry();
                        } else {
                            saveData(ptr);
                        } break;

                        // Load-Funktion wird erst nach Bestätigung durchgeführt, falls bereits Einträge bestehen
                case 5: if (ptr) {
                            P:
                            printf("Dieser Vorgang l\x94scht die bisherigen Eintr\x84ge. Wollen Sie vorher ihre Daten abspeichern?\n(1)-Ja\n(2)-Nein\n");
                            scanf("%d", &input);
                            if (input == 1){
                                saveData(ptr);
                            } else if (input != 2){
                                printf("Falsche Eingabe!\nEnter um fortzufahren");
                                fflush(stdin);
                                while( getchar() != '\n' );
                                printf("\n\n\n"); 
                                fflush(stdin);
                                goto P;
                            }
                        }    
                           ptr = loadFile(ptr);
                           break;
                        
                        break;

                        // Sortierfunktion
                case 6: if (ptr == NULL){
                            noEntry();                      
                        } else {
                            sortStudents(ptr);
                        }
                        break;

                        // Beenden des Programms
                case 7: return;

                        // "Falsche Eingabe"-Option
                default: printf("Falsche Eingabe!\nEnter um fortzufahren");
                        input = 0;
                        fflush(stdin);
                        while( getchar() != '\n' );
                        printf("\n\n\n"); 
                        fflush(stdin);
                        break;
            }
    }
}